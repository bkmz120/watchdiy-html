var WatchCanvas = angular.module('WatchApp.WatchCanvas', []);

WatchCanvas.service('WatchCanvas', function() {
	this.init = function() {
		this.canvas = new fabric.CanvasEx('watchcanvas', {
			preserveObjectStacking: true,
			renderOnAddRemove: false,
			selection: false,
			backgroundColor: "rgba(247, 247, 247,0.56)",
		});
		var self = this;
	}

	this.renderAll = function() {
		this.canvas.getObjects().sort(function(a, b) {
			if (a.weight > b.weight) {
				return 1;
			} else {
				return -1;
			}
			return 0;
		});
		this.canvas.renderAll();
	}

	this.getCanvasImage = function() {
		return this.canvas.toDataURL("image/png", 1);
	}

});

WatchCanvas.directive('watchcanvas', ['WatchCanvas', function(WatchCanvas) {
	return {
		template: "<canvas id='watchcanvas'  width=500 height=500></canvas>",
		replace: false,
		scope: {},
		restrict: 'E',

		link: function(scope, element, attrs) {

		}

	}
}]);