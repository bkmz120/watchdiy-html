angular.module('WatchApp.WatchCanvasComponents')
  .service('WatchStrap',WatchStrap);

WatchStrap.$inject = ['WatchCanvas','WatchCase'];

function WatchStrap(WatchCanvas,WatchCase) {
	this.wStrap = null;

  this.allStraps = [];

	this.fabObStrap = null;	

	this.getStrapName = function() {
		if (this.wStrap!=null){
			return this.wStrap.name;
		}
		return null;
	}

	this.getTitle = function() {
		if (this.wStrap!=null){
			return this.wStrap.title;
		}
		return null;
	}

	this.getPrice = function() {
		if (this.wStrap!=null){
			return this.wStrap.price;
		}
		return null;
	}

	this.mouseHoverPrice = null; // сюда записывается цена элемента на котором
								 // находится мышь. Нужно для расчета надбавочной цены
	this.getMouseHoverPrice = function() {
		if (this.mouseHoverPrice!=null) {
			return this.mouseHoverPrice;
		}
		else {
			return this.wStrap.price;
		}
	}

	/**
	 * Установить ремешок
	 * @param {object} wStrap информация о циферблате
	 */
	this.setStrap = function(wStrap,loadedFromServer) {

		this.wStrap = wStrap;

		if (WatchCase.wCase!=null)
		{
			this.topOffset = 0;
			this.leftOffset = 0;

			var self = this;

			fabric.Image.fromURL(this.wStrap.img, function(oImg) {
				oImg.set('top',self.topOffset);
				oImg.set('left',self.leftOffset);
				oImg.set('selectable', false);
				oImg.set('weight',-2);
				WatchCanvas.canvas.add(oImg);
				if (self.fabObStrap!=null) {
					WatchCanvas.canvas.remove(self.fabObStrap);
				}
				self.fabObStrap = oImg;
				WatchCanvas.renderAll();				

			});
		}
		else
		{
			console.log('Warning: case is empty');
		}

	}	

}
