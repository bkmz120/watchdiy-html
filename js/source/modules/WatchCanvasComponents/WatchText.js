angular.module('WatchApp.WatchCanvasComponents')
  .service('WatchText', WatchText)
  .factory("injectCSS", injectCSS);

WatchText.$inject = ['$q', 'Store', 'WatchCanvas', 'WatchCase', 'WatchFace', 'injectCSS'];
injectCSS.$inject = ['$q', '$http'];

function WatchText($q, Store, WatchCanvas, WatchCase, WatchFace, injectCSS) {
  //properties
  this.fabObText = null;
  this.fabObBorder = null;
  this.transformMode = false;
  this.editTextMode = false;

  this.allFonts = []; //все шрифты допустимые для текущей конфигурации конструктора часов (шрифты зависят от циферблата)
  this.allColors = []; //все цвета (их набор постянен, все из fontcolorsConf)

  this.wFont = null; // текущий шрифт (один из шрифтов, доступный в fontConf)
  this.fontSize = null; //текущий размер шрифта (int)
  this.color = null; //текущий цвет шрифта (string hex value)
  this.wText = null; //объект хранящий в себе текст, и координаты
  this.textState = null; //сотояние текста, для сохранения на сервере
  this.needUpdateInput = false; //флаг указывающий на то что нужно обновить поле ввода текста, используется в
                                //CanvasComponents.changeText() и в textcontrol.js

  //methods
  this.getText = getText;
  this.getTextStyle = getTextStyle;
  this.getFontName = getFontName;
  this.saveTextState = saveTextState;
  this.getTextState = getTextState;
  this.loadState = loadState;
  this.createNewText = createNewText;
  this.setAllFonts = setAllFonts;
  this.setFont = setFont;
  this.setFontSize = setFontSize;
  this.setColor = setColor;
  this.changeText = changeText;
  this.setWText = setWText;
  this.transformModeOn = transformModeOn;
  this.transformModeOff = transformModeOff;
  this.editTextModeOn = editTextModeOn;
  this.editTextModeOff = editTextModeOff;
  this.delete = deleteText;
  this.addLineBreaks = addLineBreaks;

  //ширина и высота прямоуголника по центру холста, за пределы которого текст нельзя переместить
  var allowableRectWidth = 300;
  var allowableRectHeight = 300;
  var allowableRect;


  function getText() {
    if (this.wText != null) return this.wText.text;
    else return null;
  }

  function getTextStyle() {
    if (this.wText != null) {
      var textStyle = this.getFontName() + " Размер: " + this.fontSize + " Цвет: " + this.color;
      return textStyle;
    }
    else return null;
  }

  function getFontName() {
    if (this.wFont != null) {
      return this.wFont.name;
    } else {
      return null;
    }
  }

  function saveTextState() {
    if (this.wText != null) {
      this.textState = {
        text: this.wText.text,
        fontName: this.wFont.name,
        fontSize: this.fontSize,
        color: this.color,
        top: this.fabObText.top,
        left: this.fabObText.left,
      }
    } else this.textState = null;
  }

  function getTextState() {
    return this.textState;
  }

  function loadState(textState) {
    this.textState = null;
    this.wFont = Store.getFontByName(textState.fontName);
    this.fontSize = textState.fontSize;
    this.color = textState.color;
    
    var wText = {
      text: textState.text,
      top: textState.top,
      left: textState.left,
    }

    this.setWText(wText);
  }

  function createNewText() {
    var wText = {
      text: 'Ваше имя',     
    }
    this.setWText(wText);
  }

  function setAllFonts(fonts) {
    var deferred = $q.defer();
    var self = this;
    // injectCSS.set(fonts).then(function() {
    //  self.allFonts = fonts;      
    //  deferred.resolve();

    // });
    self.allFonts = fonts;
    deferred.resolve();
    return deferred.promise;
  }

  function setFont(wFont) {
    this.wFont = wFont;
    //проверим находится ли текщуйи размер шрфита в списке допустимых размеров для данного шрифта
    var notFound = true;
    for (var i = 0; i < this.wFont.sizes.length; i++) {
      if (this.wFont.sizes[i] == this.fontSize) {
        notFound = false;
        break;
      }
    }
    if (notFound) {
      // ставим 3 с начала размер
      if (this.wFont.sizes.length>=3) {
        this.fontSize = this.wFont.sizes[2];
      }
      else {
        this.fontSize = this.wFont.sizes[0];
      }     
    }

    if (this.wText != null && this.fabObText!=null) {
      this.fabObText.set('fontFamily', this.wFont.name);
      this.fabObText.set('fontSize', convertPtToPx(this.fontSize));
      WatchCanvas.canvas.renderAll();
      this.saveTextState();
    }
  }

  function setFontSize(fontSize) {
    this.fontSize = fontSize;
    if (this.wText != null && this.fabObText!=null) {

      this.fabObText.set('fontSize', convertPtToPx(this.fontSize));
      WatchCanvas.canvas.renderAll();
      this.saveTextState();
    }
  }

  function setColor(color) {
    this.color = color;
    if (this.wText != null && this.fabObText!=null) {
      this.fabObText.setColor(this.color);
      WatchCanvas.canvas.renderAll();
      this.saveTextState();
    }
  }

  function changeText(text) {
    if (this.wText==null) {
      this.createNewText();
    }
    else {
      this.wText.text = text;
      this.fabObText.setText(text);
      this.fabObText._initDimensions();
      WatchCanvas.canvas.renderAll();
      this.saveTextState();
    }
    
    //_initDimensions() 
  }

  /**
   * Установить текст
   * @param {object} wText информация о тексте - сам текст, координаты, размеры (scaleX,scaleY)
   */
  function setWText(wText) {
    if (allowableRect==null) {
      createAllwableRect();
    }
    
    this.wText = wText;

    var self = this;
    
    console.log('self.fontSize',self.fontSize);
    self.fabObText = new fabric.IText(wText.text, {
      fontStyle: 'normal',
      fontFamily: self.wFont.name,
      fontSize: convertPtToPx(self.fontSize),
      selectionStart: 0,
      selectionEnd: wText.text.length,
      lineHeight:0.85,
      textAlign:"center",
    });

    self.fabObText.setColor(self.color);

    self.fabObText.set('weight', -3);

    self.fabObText.setControlsVisibility({
      tl: false,
      tr: false,
      bl: false,
      br: false,
      mt: false,
      mb: false,
      ml: false,
      mr: false,
    });

    if (wText.top != null) {
      self.fabObText.set('top', wText.top);
    } else {
      //self.fabObText.set('top', WatchFace.fabObFaceFull.top + WatchFace.fabObFaceFull.height / 2 + 60);
      self.fabObText.set('top', 310);
      self.wText.top = 310;
    }
    if (wText.left != null) {
      self.fabObText.set('left', wText.left);
    } else {
      //self.fabObText.set('left', WatchFace.fabObFaceFull.left + WatchFace.fabObFaceFull.width / 2);
      self.fabObText.set('left', 250);
      self.wText.left = 250;
    }

    self.fabObText.set('originX', 'center');
    self.fabObText.set('originY', 'center');
    self.fabObText.set('selectable', false);
    self.fabObText.set('lockMovementX', true);
    self.fabObText.set('lockMovementY', true);
    self.fabObText.set('lockRotation', true);

    WatchCanvas.canvas.add(self.fabObText);

    // WatchCanvas.canvas.on("text:editing:exited", function(e) {
    //  self.editTextModeOff();
    // });

    WatchCanvas.canvas.on("text:changed", function(e) {
      self.wText.text = self.fabObText.text;
    });

    WatchCanvas.renderAll();
    this.saveTextState();
    //debugger;
  }

  function transformModeOn() {
    this.transformMode = true;
    WatchFace.setHoleHilight();

    var self = this;

    self.fabObBorder = new fabric.Rect({
      left: self.fabObText.left,
      top: self.fabObText.top,
      fill: 'rgba(0,0,0,0)',
      stroke: '#3066f1',
      strokeWidth: 1,
      width: self.fabObText.width,
      height: self.fabObText.height,
      scaleX: self.fabObText.scaleX,
      scaleY: self.fabObText.scaleY,
      angle: self.fabObText.angle,
      originX: 'center',
      originY: 'center',
      weight: 1,
      selectable: true,
    });

    self.fabObBorder.setControlsVisibility({
      tl: false,
      tr: false,
      bl: false,
      br: false,
      mt: false,
      mb: false,
      ml: false,
      mr: false,
    });


    WatchCanvas.canvas.setActiveObject(self.fabObBorder);

    self.fabObBorder.on('deselected', function() {
      setTimeout(function() {
        WatchCanvas.canvas.setActiveObject(self.fabObBorder);
      }, 0);
    });

    // активировать редактирование текста по двойному клику
    // self.fabObBorder.on('object:dblclick', function() {
    //  self.editTextModeOn();
    // });

    var goodtop, goodleft, goodangle;

    self.fabObBorder.on('moving', function() {
      self.fabObBorder.setCoords();
      if (self.fabObBorder.isContainedWithinObject(allowableRect)) {
        self.fabObText.left = self.fabObBorder.left;
        self.fabObText.top = self.fabObBorder.top;
        goodleft = self.fabObBorder.left;
        goodtop = self.fabObBorder.top;
      } else {
        self.fabObBorder.left = goodleft;
        self.fabObBorder.top = goodtop;
      }

    });

    self.fabObBorder.on('rotating', function() {
      self.fabObBorder.setCoords();
      if (self.fabObBorder.isContainedWithinObject(allowableRect)) {
        self.fabObText.angle = self.fabObBorder.angle;
        goodangle = self.fabObBorder.angle;
      } else {
        self.fabObBorder.angle = goodangle;
      }
    });

    WatchCanvas.canvas.add(self.fabObBorder);
    WatchCanvas.renderAll();
  }

  function transformModeOff() {
    this.transformMode = false;

    if (this.fabObBorder != null) {
      // if (this.editTextMode) {
      //  this.editTextModeOff();
      // }
      WatchFace.unsetHoleHilight();
      WatchCanvas.canvas.remove(this.fabObBorder);
      WatchCanvas.renderAll();
      this.saveTextState();
    }
  }

  function editTextModeOn() {
    this.editTextMode = true;
    this.fabObBorder.set('strokeWidth', 0);
    this.fabObBorder.off('deselected');
    WatchCanvas.canvas.setActiveObject(this.fabObText);
    this.fabObText.set('weight', 1);
    WatchCanvas.renderAll();
    this.fabObText.enterEditing();
    this.fabObText.hiddenTextarea.focus();
    this.fabObText.hiddenTextarea.select();
  }

  function editTextModeOff() {
    this.editTextMode = false;
    this.fabObBorder.set('strokeWidth', 1);
    this.fabObBorder.set('width', this.fabObText.width);
    this.fabObBorder.set('height', this.fabObText.height);
    var self = this;

    WatchCanvas.canvas.setActiveObject(self.fabObBorder);

    this.fabObBorder.on('deselected', function() {
      setTimeout(function() {
        WatchCanvas.canvas.setActiveObject(self.fabObBorder);
      }, 0);
    });
    this.fabObText.set('weight', -3);
    WatchCanvas.renderAll();
  }

  function deleteText() {
    if (this.transformMode) {
      this.transformModeOff();
    }

    if (this.wText != null  && this.fabObText!=null) {
      this.fabObText.remove();
      this.wText = null;
      //ServerState.saveWatchComponentState('text',this.getTextInfo());
      WatchCanvas.renderAll();
      this.saveTextState();
    }

  }

  function createAllwableRect() {
    allowableRect = new fabric.Rect({
      left: (WatchCanvas.canvas.width - allowableRectWidth)/2,
      top: (WatchCanvas.canvas.height - allowableRectHeight)/2,   
      fill: 'rgba(0,0,0)',
      width: allowableRectWidth,
      height: allowableRectHeight,
    });
  }

  function convertPtToPx(valuePt) {
    return valuePt*4/3;
  }

  /**
   * Добавить пеносы в текст так, что бы каждая строка не превышала _maxStrLen
   * (не выходила за пределы циферблата)
   * @param {number} _maxStrLen
   */
  function addLineBreaks(_text,_maxStrLen) {
    if (_text.length<=_maxStrLen) return _text;
    //если уже есть переносы, то ничего не делаем
    if (_text.indexOf("\n")!=-1) return _text;
    
    var rows = [],
      rowIndex = 0,
      words;

    words = _text.split(" ");   
    rows[0] = "";
    for (var i=0;i<words.length;i++) {
      if (words[i].length!=0) {
        if (rows[rowIndex].length+words[i].length+1 > _maxStrLen) {
          rowIndex++;
          rows[rowIndex]="";
        }
        if (rows[rowIndex].length==0) {
          rows[rowIndex] = words[i];
        }
        else {
          rows[rowIndex] = rows[rowIndex] + " " + words[i];
        }
      }     
    }

    return rows.join("\r\n");
  }
}

function injectCSS($q, $http) {
  var injectCSS = {};

  var createLink = function(url) {
    var link = document.createElement('link');
    link.rel = "stylesheet";
    link.type = "text/css";
    link.href = url;
    return link;
  }

  var checkLoaded = function(urls, deferred, tries) {

    var allExists = true;
    for (var i in urls) {
      var notFound = true;
      for (var j in document.styleSheets) {
        var href = document.styleSheets[j].href || "";
        if (href.split("/").slice(-1).join() === urls[i].split("/").slice(-1).join()) {
          notFound = false;
        }
      }
      if (notFound) {
        allExists = false;
        break;
      }
    }

    console.log('allExists:',allExists);

    if (allExists) {

      deferred.resolve();
      return;
    }
    tries++;
    setTimeout(function() {
      checkLoaded(urls, deferred, tries);
    }, 500);
  };

  var findNotExistUrls = function(fonts) {
    var notExist = [];

    for (var i in fonts) {
      var notFound = true;
      for (var j in document.styleSheets) {
        var href = document.styleSheets[j].href || "";
        if (href.split("/").slice(-1).join() === fonts[i].url.split("/").slice(-1).join()) {
          notFound = false;
        }
      }
      if (notFound) {
        notExist.push(fonts[i].url);
      }
    }

    return notExist;
  }

  injectCSS.set = function(fonts) {
    var deferred = $q.defer();
    var notExistUrls = findNotExistUrls(fonts);

    for (var i in notExistUrls) {
      var link = createLink(notExistUrls[i]);
      angular.element(document.getElementsByTagName('head')[0]).append(link);
    }

    var tries = 0;
    checkLoaded(notExistUrls, deferred, tries);
    return deferred.promise;
  };

  return injectCSS;
}