angular.module('WatchApp.WatchControlComponents')
  .component('textInputPopup', {
    controller:textInputPopup,
    controllerAs: 'vm',
    templateUrl:'/js/modules/WatchControlComponents/templates/textInputPopup.html',
  });

textcontrol.$inject = ['$scope','$element','CanvasComponents','WatchText','ConstructorTips'];

function textInputPopup($scope,$element,CanvasComponents,WatchText,ConstructorTips) {
  var vm=this,
    userTextInputEl,
    bgEl,
    popUpEl;
  vm.onComplite = onComplite;
  vm.btnActive = false;
  vm.visible = false;
  init();

  function init() {
    userTextInputEl = angular.element($element[0].querySelector('.textInputPopup__textinp'));
    userTextInputEl.on('keyup paste',onUserTextChanged);    
    initScopeWatchs();
  }

  function initScopeWatchs() {
    $scope.$watch(
      function(){
        if (ConstructorTips.isActive){
          return ConstructorTips.getCurrentTip();
        }
        else {
          return null;
        }  
      },
      function(newValue,oldValue){
        if (newValue=="textInputPopup" && ConstructorTips.isActive && !vm.visible){
          show();  
        }      
      }
    );    
  }

  function show() {
    vm.visible = true;
    setTimeout(function(){
      userTextInputEl[0].focus();
    },800);
  }

  function close() {
    ConstructorTips.nextTip();
    vm.visible = false;
  }

  function onUserTextChanged(e) {
    if (e.keyCode == 13 ) {
      onComplite();
    }
    if (userTextInputEl.val()=="") {
      vm.btnActive = false;
      $scope.$digest();
    }
    else {
      vm.btnActive = true;
      $scope.$digest();
    }
  }

  function onComplite() {
    if (!vm.btnActive) return;    
    var newText = userTextInputEl.val();
    CanvasComponents.changeText(newText,true,true);
    close();
  }

}