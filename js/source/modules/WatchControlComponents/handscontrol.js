angular.module('WatchApp.WatchControlComponents')
  .component('handscontrol', {
    controller:handscontrol,
    controllerAs: 'vm',
    templateUrl:'/js/modules/WatchControlComponents/templates/handscontrol.html',
  });

handscontrol.$inject = ['$scope','$element','CanvasComponents','WatchHands'];

function handscontrol($scope,$element,CanvasComponents,WatchHands) {
  var vm = this;

  vm.allHands = null;
  vm.selectedHandsName = null;

  vm.changeHands= function(item) {
    CanvasComponents.changeHands(item);
    vm.selectedHandsName = item.name;
    WatchHands.mouseHoverPrice = null;
    CanvasComponents.calcMouseHoverAddPrice();
  }

  vm.mouseOnItem = function(item) {
    if (item!=null) {
      WatchHands.mouseHoverPrice = item.price;
    }
    else {
      WatchHands.mouseHoverPrice = null;
    }
    CanvasComponents.calcMouseHoverAddPrice();
  }


  $scope.$watch(
    function(){
      return WatchHands.getHandsName();
    },
    function(newValue,oldValue){
      if (newValue!=oldValue || vm.selectedHandsName==null){
        vm.selectedHandsName = newValue;
      }
    }
  );

  
  $scope.$watch(
    function(){
      return WatchHands.allHands;
    },
    function(newValue,oldValue){
      if (newValue!=oldValue || vm.allHands==null ){
        vm.allHands = WatchHands.allHands;        
      }
    },
    true
  );

}
