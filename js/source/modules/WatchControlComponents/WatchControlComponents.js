/**
 * Модуль с директивами для управения конструктором часов
 */
angular.module('WatchApp.WatchControlComponents', 
                [
                 'WatchApp.WatchCanvasComponents',
                 'WatchApp.ConstructorTips',
                 'mp.colorPicker'
                ]
              );