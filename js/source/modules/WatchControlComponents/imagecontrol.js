angular.module('WatchApp.WatchControlComponents')
  .component('imagecontrol', {
    controller:imagecontrol,
    controllerAs: 'vm',
    templateUrl:'/js/modules/WatchControlComponents/templates/imagecontrol.html',
  });

imagecontrol.$inject = ['$scope','CanvasComponents','WatchImage','WatchText'];

function imagecontrol($scope,CanvasComponents,WatchImage,WatchText) {
  var vm=this;

  vm.controlStatus = "add";
  vm.imageUrl = null;

  vm.upload = function(file) {
    WatchImage.uploadImage(file).then(function(fileInfo){
      vm.imageUrl = fileInfo.fileUrl;
      CanvasComponents.addImage(fileInfo.fileUrl,fileInfo.fileName);
    });
  }

  vm.transformModeOnBtnVisible = false;

  vm.transformModeOn = function() {
    if (vm.transformModeOnBtnVisible) {
      if (WatchText.transformMode) {
        WatchText.transformModeOff();
      }
      WatchImage.transformModeOn();
      vm.transformModeOnBtnVisible = false;
    }
  }

  //что бы при загрузке сохраненного изображени был включен режим edit
  $scope.$watch(
    function(){
      return WatchImage.wImage;
    },
    function(newValue, oldValue){
      if (newValue!=null ) {
        vm.imageUrl = newValue.fileUrl;
        vm.controlStatus = "edit";
      }
      if (newValue==null && vm.controlStatus != "add") {
        vm.controlStatus = "add";
      }
    }
  );

  $scope.$watch(
    function(){
      return WatchImage.transformMode;
    },
    function(newValue, oldValue){
      if (WatchImage.transformMode==false) {
        vm.transformModeOnBtnVisible = true;
      }
    }
  );

  vm.delete = function() {
    CanvasComponents.deleteImage();
    vm.controlStatus = "add";
  }
}
