angular.module('WatchApp.WatchControlComponents')
  .component('facecontrol', {
    controller:facecontrol,
    controllerAs: 'vm',
    templateUrl:'/js/modules/WatchControlComponents/templates/facecontrol.html',
  });

facecontrol.$inject = ['$scope','$element','CanvasComponents','WatchFace'];

function facecontrol($scope,$element,CanvasComponents,WatchFace) {
  var vm = this;

  vm.allFaces = null;
  vm.selectedFaceName = null;
  


  vm.changeFace = function(item) {
    CanvasComponents.changeFace(item);
    vm.selectedFaceName = item.name;
    WatchFace.mouseHoverPrice = null;
    CanvasComponents.calcMouseHoverAddPrice();
  }

  vm.mouseOnItem = function(item) {
    if (item!=null) {
      WatchFace.mouseHoverPrice = item.price;
    }
    else {
      WatchFace.mouseHoverPrice = null;
    }
    CanvasComponents.calcMouseHoverAddPrice();
  }


  $scope.$watch(
    function(){
      return WatchFace.getFaceName();
    },
    function(newValue,oldValue){
      if (newValue!=oldValue || vm.selectedFaceName==null){
        vm.selectedFaceName = newValue;
      }
    }
  );  
  

  $scope.$watch(
    function(){
      return WatchFace.allFaces;
    },
    function(newValue,oldValue){
      if (newValue!=oldValue || vm.allFaces==null ){
        vm.allFaces = WatchFace.allFaces;        
      }
    },
    true
  );

  

}
