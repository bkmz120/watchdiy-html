angular.module('WatchApp.WatchControlComponents')
  .component('compliteImageTransform', {
    controller:compliteImageTransform,
    controllerAs: 'vm',
    templateUrl:'/js/modules/WatchControlComponents/templates/compliteImageTransform.html',
  });

compliteImageTransform.$inject = ['$scope','WatchImage'];

function compliteImageTransform($scope,WatchImage) {
  var vm=this;
  vm.complite = function(){
    WatchImage.transformModeOff();
  }

  vm.visible = false;

  $scope.$watch(
    function(){
      return WatchImage.transformMode;
    },
    function(newValue, oldValue){
      if (newValue==true) {
        vm.visible = true;
      }
      else {
        vm.visible = false;
      }
    }
  );
}
