angular.module('WatchApp.WatchControlComponents')
	.component('controlItems', {
		controller: controlItems,
		controllerAs: 'vm',
		templateUrl: '/js/modules/WatchControlComponents/templates/controlItems.html',
		bindings: {
			items: '<',
			selectedName:'<',
			selectCallback:'<',
			mouseonitemCallback:'<',
			imgClass:'@',
		}
	});

controlItems.$inject = ['$scope', '$element'];

function controlItems($scope, $element) {
	var vm = this;
	vm.scrollActive = false;
	vm.scrollRight = scrollRight;
	vm.scrollLeft = scrollLeft;
	vm.changeSelectItem = changeSelectItem;
	vm.mouseEnterItem = mouseEnterItem;
	vm.mouseLeaveItem = mouseLeaveItem;

	var controlItemsInnerEl = angular.element($element[0].querySelector('.constructor-control_items-inner'));
	var controlItemsNode = $element[0].querySelector('.constructor-control_items');
	var controlItemsRightArrowEl = angular.element($element[0].querySelector('.constructor-control_items-rightarrow'));
	var controlItemsLeftArrowEl = angular.element($element[0].querySelector('.constructor-control_items-leftarrow'));
	var mCSB_containerNode;
	var deltaWidth;

	this.$onChanges = function(changesObj) {
		if (changesObj.items!=null && changesObj.items.currentValue!=null) {
			updateScroll();
		}
		if (changesObj.selectedName) {

		}		
	}

	function updateScroll() {
		if (vm.items.length > 3) {
			vm.scrollActive = true;
			controlItemsRightArrowEl.addClass('constructor-control_items-rightarrow__visible');
		}
		else {
			controlItemsRightArrowEl.removeClass('constructor-control_items-rightarrow__visible');
			controlItemsLeftArrowEl.removeClass('constructor-control_items-leftarrow__visible');
		}

		var controlWidth = vm.items.length * 76;
		deltaWidth = controlWidth - controlItemsNode.offsetWidth;

		controlItemsInnerEl.css('width', controlWidth + 'px');
		$(controlItemsNode).mCustomScrollbar("destroy");
		$(controlItemsNode).mCustomScrollbar({
			axis: "x",
			scrollInertia: 0,
			callbacks: {
				onTotalScroll: function() {

				},
				onTotalScrollBack: function() {

				},
				whileScrolling: function() {
					var leftOffset = this.mcs.left;
					if (leftOffset >= -10) {
						controlItemsLeftArrowEl.removeClass('constructor-control_items-leftarrow__visible');
					} else {
						controlItemsLeftArrowEl.addClass('constructor-control_items-leftarrow__visible');
					}

					if (-leftOffset > deltaWidth - 10) {
						controlItemsRightArrowEl.removeClass('constructor-control_items-rightarrow__visible');
					} else {
						controlItemsRightArrowEl.addClass('constructor-control_items-rightarrow__visible');
					}

				},
			}
		});
	}

	function scrollRight() {
		if (vm.scrollActive) {
			mCSB_containerNode = $element[0].querySelector('.mCSB_container');
			var currentOffset = -parseInt(mCSB_containerNode.style.left);
			var newOffset = currentOffset + 120;
			if (newOffset > deltaWidth) newOffset = deltaWidth;
			$(controlItemsNode).mCustomScrollbar("scrollTo", newOffset, {
				scrollInertia: 200
			});
		}
	}

	function scrollLeft() {
		if (vm.scrollActive) {
			mCSB_containerNode = $element[0].querySelector('.mCSB_container');
			var currentOffset = -parseInt(mCSB_containerNode.style.left);
			var newOffset = currentOffset - 120;
			if (newOffset < 0) newOffset = 0;
			$(controlItemsNode).mCustomScrollbar("scrollTo", newOffset, {
				scrollInertia: 200
			});
		}
	}

	function changeSelectItem(item) {
		vm.selectCallback(item);
	}

	function mouseEnterItem(item) {
		if (vm.selectedName!=item.name && vm.mouseonitemCallback!=null) {
			vm.mouseonitemCallback(item);
		}		
	}

	function mouseLeaveItem(item) {
		if (vm.selectedName!=item.name && vm.mouseonitemCallback!=null) {
			vm.mouseonitemCallback(null);
		}
	}

}