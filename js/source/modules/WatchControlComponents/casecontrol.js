angular.module('WatchApp.WatchControlComponents')
  .component('casecontrol', {
    controller:casecontrol,
    controllerAs: 'vm',
    replace:true,
    templateUrl:'/js/modules/WatchControlComponents/templates/casecontrol.html',
  });

casecontrol.$inject = ['$scope','$element','CanvasComponents','WatchCase'];

function casecontrol($scope,$element,CanvasComponents,WatchCase) {
  var vm = this;

  vm.allCases = null;
  vm.selectedCaseName = null;

  vm.changeCase = function(item) {
    CanvasComponents.changeCase(item);
    vm.selectedCaseName = item.name;
    WatchCase.mouseHoverPrice = null;
    CanvasComponents.calcMouseHoverAddPrice();    
  }

  vm.mouseOnItem = function(item) {
    if (item!=null) {
      WatchCase.mouseHoverPrice = item.price;
    }
    else {
      WatchCase.mouseHoverPrice = null;
    }
    CanvasComponents.calcMouseHoverAddPrice();
  }
 
  $scope.$watch(
    function(){
      return WatchCase.allCases;
    },
    function(newValue,oldValue){

      if (newValue!=oldValue || vm.allCases==null){
        vm.allCases = WatchCase.allCases;         
      }
    }
  );

  //следит за тем, не установили ли новый корпус
  $scope.$watch(
    function(){
      return WatchCase.getCaseName();
    },
    function(newValue,oldValue){
      if (newValue!=oldValue || vm.selectedCaseName==null){
        vm.selectedCaseName = newValue;
      }

    }
  );



}

