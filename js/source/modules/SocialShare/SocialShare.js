angular.module('WatchApp.SocialShare', ['WatchApp.Order'])
	.service('SocialShare',SocialShare);

SocialShare.$inject = ['$http','$q','Order'];

function SocialShare($http,$q,Order) {
	this.vkontakte = vkontakte;
	this.facebook = facebook;
	this.twitter = twitter;

	var ptitle = "Именные наручные часы онлайн конструктор";
	var purl = 'http://watchn1.ru';
	var ptext = "Создайте свои часы в онлайн конструторе."
	var pimg = "http://watchn1.ru/img/clock.png";

	function vkontakte (orderId) {
		url  = 'https://vk.com/share.php?';
		url += 'url='          + encodeURIComponent(purl);
		url += '&title='       + encodeURIComponent(ptitle + ' ' + createHashTags(orderId));
		url += '&description=' + encodeURIComponent(ptext);
		url += '&image='       + encodeURIComponent(pimg);
		url += '&noparse=true';
		var deferred = $q.defer();
		popup(url);
		Order.sendRepostNotice('vkontakte');
		deferred.resolve();
		return deferred.promise;
	}

	function facebook(orderId) {
		var deferred = $q.defer();
		initFacebookApi().then(function(){
			FB.ui({
				method: 'feed',
				link: purl,
				picture:pimg,
				caption: createHashTags(orderId),
				description:'онлайн конструктор',
			}, function(response){
				deferred.resolve();
				Order.sendRepostNotice('facebook');
			});
		});

		return deferred.promise;
	}

	function twitter(orderId) {
		url  = 'http://twitter.com/share?';
		url += 'text='      + encodeURIComponent(ptitle);
		url += '&url='      + encodeURIComponent(purl);
		url += '&hashtags=' + encodeURIComponent(createHashTags(orderId,true));
		var deferred = $q.defer();
		popup(url);
		Order.sendRepostNotice('twitter');
		deferred.resolve();
		return deferred.promise;
	}


	function createHashTags(orderId,twitMode) {
		if (twitMode===true) {
			return 'watchn1ru,номерзаказа'+orderId;
		}
		else
		{
			return "#watchn1ru #номерзаказа"+orderId;
		}
		
	}

	function popup(url) {
		window.open(url,'','toolbar=0,status=0,width=626,height=436');		
	}

	function initFacebookApi() {
		var deferred = $q.defer();
		if (typeof FB === "undefined") { 
			console.log('start init FB');
			window.fbAsyncInit = function() {
				FB.init({
					appId: '1224158137704818',
					xfbml: true,
					version: 'v2.8'
				});
				FB.AppEvents.logPageView();
				deferred.resolve();
			};

			(function(d, s, id) {
				var js, fjs = d.getElementsByTagName(s)[0];
				if (d.getElementById(id)) {
					return;
				}
				js = d.createElement(s);
				js.id = id;
				js.src = "//connect.facebook.net/en_US/sdk.js";
				fjs.parentNode.insertBefore(js, fjs);
			}(document, 'script', 'facebook-jssdk'));
		}
		else {
			deferred.resolve();
		}

		return deferred.promise;
	}



}