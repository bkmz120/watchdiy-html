angular.module('WatchApp.AskForm', ['WatchApp.Order'])
	.component('askform', {
	    controller:askform,
	    controllerAs: 'vm',
	    templateUrl:'/js/modules/AskForm/askform.html',
	  });

askform.$inject = ['$scope','Order'];

function askform($scope,Order) {
  var vm=this;  

  vm.state = "form";


  vm.sendForm = function() {
    validate();
  }

  vm.values = {
    name:"",
    email:"",
    text:"",
  }

  vm.validResults = {
    name:true,
    email:true,
    text:true,
  }

  //атовразмер для поля ввода адреса в форме (autosize.min.js)
  autosize(document.querySelector('.askForm_text'));

  function validate() {
    vm.validResults = {
      name:true,
      email:true,
      text:true,
    }
    var valid = true;

    if (vm.values.name==null || vm.values.name=="") {
      vm.validResults.name = false;
      valid = false;
    }

    var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    
    if (vm.values.email==null || vm.values.email=="" || !re.test(vm.values.email)) {
      vm.validResults.email = false;
      valid = false;
    }

    if (vm.values.text==null || vm.values.text=="") {
      vm.validResults.text = false;
      valid = false;
    }

    if (valid) {
      send();
    }
  }


  function send() {
  	vm.state = "loading";
  
    Order.sendAskForm(vm.values).then(
      //success
      function(data){      
        vm.state = "success";
        vm.values = {
          name:"",
          email:"",
          text:"",
        }

        vm.validResults = {
          name:true,
          email:true,
          text:true,
        }
      },
      //fail
      function() {
        console.log('fail');
      }
    );
  }
}