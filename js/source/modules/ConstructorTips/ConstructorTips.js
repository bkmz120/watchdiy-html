angular.module('WatchApp.ConstructorTips', [])
	.service('ConstructorTips', ConstructorTips);

ConstructorTips.$inject = [];

function ConstructorTips() {
	this.isActive = false;
	this.showTips = showTips;	
	this.getCurrentTip = getCurrentTip;
	this.nextTip = nextTip;
	this.reset = reset;

	var tips = ["textInputPopup","fonts"],
		currentTipIndex;

	function showTips(_tipName) {
		
		if (_tipName==null) {
			currentTipIndex = 0;
			this.isActive = true;
		}
		else {
			for (var i=0;i<tips.length;i++) {
				if (tips[i]==_tipName) {
					currentTipIndex = i;
					this.isActive = true;
					break;
				}
			}
		}
		
	}

	function getCurrentTip() {
		return tips[currentTipIndex];
	}

	function nextTip() {
		currentTipIndex++;		
	}

	function reset() {
		currentTipIndex = 0;
		this.isActive = false;
	}
}