angular.module('WatchApp.Config').constant("CasesConf", {
  items:[
		{
			name:'casual',
			title:'Стиль классический, часовая сталь 316L, цвет – хром',
			price:5530,
			configModes:['male','female'],
			img:"/watch-elements/cases/watch-case_casual.png",
			previewImg:"/watch-elements/cases/previews/watch-case_casual2.png",
		}
	]
});
