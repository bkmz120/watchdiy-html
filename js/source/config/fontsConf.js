angular.module('WatchApp.Config').constant("FontsConf", {
  items:[    
    {
      name:'Copperplate',
      validFaces:['rim-black','rim-white','rim-metal','arab-white','arab-metal'],
      sizes:[10,11,12,13,14,15,16,17,18,19,20,21,22],
    },
    {
      name:'Calligraphia',
      validFaces:['rim-black','rim-white','rim-metal','arab-white','arab-metal'],
      sizes:[10,11,12,13,14,15,16,17,18,19,20,21,22],
    },    
    {
      name:'ComicSans',
      validFaces:['rim-black','rim-white','rim-metal','arab-white','arab-metal'],
      sizes:[10,11,12,13,14,15,16,17,18,19,20,21,22],
    },
    {
      name:'Heinrich',
      validFaces:['rim-black','rim-white','rim-metal','arab-white','arab-metal'],
      sizes:[10,11,12,13,14,15,16,17,18,19,20,21,22],
    },
    {
      name:'Helvetica',
      validFaces:['rim-black','rim-white','rim-metal','arab-white','arab-metal'],
      sizes:[10,11,12,13,14,15,16,17,18,19,20,21,22],
    },
    {
      name:'Moderno',
      validFaces:['rim-black','rim-white','rim-metal','arab-white','arab-metal'],
      sizes:[10,11,12,13,14,15,16,17,18,19,20,21,22],
    },
    // {
    //   name:'Brush MTI',
    //   url:'/fonts/BrushScript/BrushScript.css',
    //   validFaces:['rim-black','rim-white','rim-metal','arab-white','arab-metal'],
    //   sizes:[15,16,17,18,19,20,21,22],
    // },
  ]
});
