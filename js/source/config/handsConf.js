angular.module('WatchApp.Config').constant("HandsConf", {
  items:[
		{
			name:'hands-knives',
     		title:"«Кинжалы» хромированные двугранные. Часы, минуты, секунды.",
    		price:750,
   			configModes:['male','female'],
			validFaces:['rim-black','rim-white','rim-metal','arab-white','arab-metal'],
			img:'/watch-elements/hands/hands-knives.png',
			previewImg:'/watch-elements/hands/previews/hands-knives1.png',
		},	
		{
			name:'hands-spear',
     		title:"«Копья» хромированные двугранные. Часы, минуты, секунды.",
      		price:750,
      		configModes:['male','female'],
			validFaces:['rim-black','rim-white','rim-metal','arab-white','arab-metal'],
			img:'/watch-elements/hands/hands-spear.png',
			previewImg:'/watch-elements/hands/previews/hands-spear1.png',
		},
		{
			name:'hands-black',
     		title:"«Классические» черные лакированные. Часы, минуты, секунды.",
      		price:450,
      		configModes:['male','female'],
			validFaces:['rim-black','rim-white','rim-metal','arab-white','arab-metal'],
			img:'/watch-elements/hands/hands-black.png',
			previewImg:'/watch-elements/hands/previews/hands-black1.png',
		},
	]

});
