$(document).ready(function(){
	$('.screen5__slider').slick({
		infinite: true,
		slidesToShow: 1,
		slidesToScroll: 1,
		prevArrow:'<img class="screen5-arrow-left__img" src="img/slider/arrow-left.png">',
		nextArrow:'<img class="screen5-arrow-right__img" src="img/slider/arrow-right.png">',
		autoplay:true,
		autoplaySpeed:5000,
		pauseOnHover:false,
	});

	$('.screen6-input__button').click(function(e){
		var name = encodeURIComponent($('.screen6-input__element').val());
		var url = "http://watchn1.ru/imennye-chasy/classic/" + name;
		window.location = url;
		e.preventDefault();
		return false;
	});

	$(".screen6-input__element").keypress(function(e) {
	    if(e.which == 13) {
	    	var name = encodeURIComponent($('.screen6-input__element').val());
	        var url = "http://watchn1.ru/imennye-chasy/classic/" + name;
			window.location = url;
	    }
	});

	var screen2ContentsEl = $('.screen2-contents');
	var screen7AnimationStatus = "clear";
	$(window).scroll(function() {
		var pos = $(window).scrollTop();
		if(pos >= 712 && pos <=1700  ) {
			if (!screen2ContentsEl.hasClass('visible')) {
				screen2ContentsEl.addClass('visible');
			}			
		}
		else {			
			if (screen2ContentsEl.hasClass('visible')) {
				screen2ContentsEl.removeClass('visible');	
			}	
		}
	
		if (pos>=5050 && screen7AnimationStatus=="clear") {
			changeScreen7TextStatus(1,true);	
			screen7AnimationStatus = "work";	
		}

		if (pos<4200 && screen7AnimationStatus == "done") {
			clearScreen7TextStatus();
			screen7AnimationStatus = "clear";	
		}
	});

	function changeScreen7TextStatus(textNum,visibility) {

		var selector = ".screen7-explanation__text_location"+textNum;
		var textEl = $(selector);
		if (visibility && !$(textEl).hasClass('screen7-explanation__text_visible')) {
			$(textEl).addClass('screen7-explanation__text_visible');
		} 

		if (!visibility && $(textEl).hasClass('screen7-explanation__text_visible')) {
			$(textEl).removeClass('screen7-explanation__text_visible');
		}

		textNum++;

		if (textNum<=4) {
			setTimeout(function(){
				changeScreen7TextStatus(textNum,true);
			},250);
		}	
		else {
			screen7AnimationStatus = "done";
		}	
	}

	function clearScreen7TextStatus() {
		for (var i=1;i<=4;i++) {
			var selector = ".screen7-explanation__text_location"+i;
			var textEl = $(selector);
			textEl.removeClass('screen7-explanation__text_visible');
		}
	}
	
});