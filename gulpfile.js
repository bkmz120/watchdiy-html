var gulp = require('gulp'); 
var templateCache = require('gulp-angular-templatecache');
var sourcemaps = require('gulp-sourcemaps');
var concat = require('gulp-concat');
var streamqueue  = require('streamqueue'); 
var postcss      = require('gulp-postcss');
var autoprefixer = require('autoprefixer');
var sass = require('gulp-sass');
var uncache = require('gulp-uncache');


gulp.task('css',function() {

   	return streamqueue({ objectMode: true },
  		gulp.src('./css/source/reset.css'),
  		gulp.src('./css/source/style.css'),
      gulp.src('./css/source/constructor.css'),
  		gulp.src('./css/source/**/*.scss').pipe(sass({outputStyle:"expanded"}).on('error', sass.logError))
    )
    .pipe(sourcemaps.init())
  	.pipe(concat('style.css'))
  	.pipe(postcss([ autoprefixer() ]))
  	.pipe(sourcemaps.write('.'))
    .pipe(gulp.dest('./css/dist'));
});

gulp.task('libs-css',function() {
  return gulp.src('./js/libs/**/*.css')
  	.pipe(concat('libs-style.css'))
    .pipe(gulp.dest('./css/dist'));
});


gulp.task('libs',function() {
  return streamqueue({ objectMode: true },
  		gulp.src('./js/libs/angular-cookies.min.js'),
  		gulp.src('./js/libs/angular-animate.min.js'),
  		gulp.src('./js/libs/fabric.canvasex.js'),
  		gulp.src('./js/libs/ng-file-upload-shim.min.js'),
  		gulp.src('./js/libs/ng-file-upload.min.js'),  		
  		gulp.src('./js/libs/customScrollbar/jquery.mCustomScrollbar.concat.min.js'),
  		gulp.src('./js/libs/slick/slick.min.js'),
  		gulp.src('./js/libs/autosize.min.js'),
      gulp.src('./js/libs/navigo.js'),
      gulp.src('./js/libs/angular-slick-carousel.min.js'),
      gulp.src('./js/libs/angular-scroll.min.js')
    )
  	.pipe(concat('libs.js'))
    .pipe(gulp.dest('./js/dist'));
});



gulp.task('templates', function () {
  return gulp.src('./js/source/modules/**/*.html')
    .pipe(templateCache({standalone:true,root:'/js/modules/'}))
    .pipe(gulp.dest('./js/dist'));
});


gulp.task('app-manual',['templates'], function() {
  return streamqueue({ objectMode: true },
  		gulp.src('./js/dist/templates.js'),
  		gulp.src('./js/source/modules/OutsideClick/outsideclick.js'),
        gulp.src('./js/source/modules/store/Store.js'),
        gulp.src('./js/source/modules/Order/Order.js'),
        gulp.src('./js/source/modules/OrderForm/OrderForm.js'),
        gulp.src('./js/source/modules/AskForm/AskForm.js'),
        gulp.src('./js/source/modules/CabinetSignin/CabinetSignin.js'),
        gulp.src('./js/source/modules/DeliveryPage/DeliveryPage.js'),
        gulp.src('./js/source/modules/WatchCanvas/WatchCanvas.js'),
        gulp.src('./js/source/modules/WatchCanvasComponents/WatchCanvasComponents.js'),
        gulp.src('./js/source/modules/WatchCanvasComponents/WatchCase.js'),
        gulp.src('./js/source/modules/WatchCanvasComponents/WatchFace.js'),
        gulp.src('./js/source/modules/WatchCanvasComponents/WatchHands.js'),
        gulp.src('./js/source/modules/WatchCanvasComponents/WatchStrap.js'),
        gulp.src('./js/source/modules/WatchCanvasComponents/WatchImage.js'),
        gulp.src('./js/source/modules/WatchCanvasComponents/WatchText.js'),
        gulp.src('./js/source/modules/WatchControlComponents/WatchControlComponents.js'),
        gulp.src('./js/source/modules/WatchControlComponents/controlItems.js'),
        gulp.src('./js/source/modules/WatchControlComponents/casecontrol.js'),
        gulp.src('./js/source/modules/WatchControlComponents/facecontrol.js'),
        gulp.src('./js/source/modules/WatchControlComponents/handscontrol.js'),
        gulp.src('./js/source/modules/WatchControlComponents/strapcontrol.js'),
        gulp.src('./js/source/modules/WatchControlComponents/imagecontrol.js'),
        gulp.src('./js/source/modules/WatchControlComponents/compliteImageTransform.js'),
        gulp.src('./js/source/modules/WatchControlComponents/textcontrol.js'),
        gulp.src('./js/source/modules/WatchControlComponents/compliteTextTransform.js'),
        gulp.src('./js/source/modules/WatchControlComponents/textInputPopup.js'),
        gulp.src('./js/source/modules/WatchControlComponents/tooltips.js'),
        gulp.src('./js/source/modules/PresetsSlider/PresetsSlider.js'),
        gulp.src('./js/source/modules/Constructor/ConstructorComponent.js'),
        gulp.src('./js/source/modules/CacheConstructorImages/CacheConstructorImages.js'),
        gulp.src('./js/source/modules/ConstructorTips/ConstructorTips.js'),
        gulp.src('./js/source/modules/Footer/footer.js'),
        gulp.src('./js/source/modules/SocialShare/SocialShare.js'),
        gulp.src('./js/source/config/config.js'),
        gulp.src('./js/libs/color-picker/angular-color-picker.js'),
        gulp.src('./js/source/app.js')
    )
  	.pipe(concat('build.js'))
    .pipe(gulp.dest('./js/dist'));
});

gulp.task('uncache', function () {
    return gulp.src('./js/source/app.html')
      .pipe(uncache({
        append: 'time',
        rename: false
      }))
      .pipe(concat('index.html'))
      .pipe(gulp.dest('.'));
});

gulp.task('build', ['templates', 'app-manual','css','libs','libs-css','uncache']);


gulp.task('default',function(){
  gulp.watch('./css/source/*.css', ['css','uncache']);
  gulp.watch('./css/source/*.scss', ['css','uncache']);
  gulp.watch('./js/source/modules/**/*.html', ['templates','app-manual','uncache']);
  gulp.watch('./js/source/**/*.js', ['app-manual','uncache']);
  gulp.watch('./js/source/app.html', ['uncache']);
});

