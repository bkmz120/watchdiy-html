<?php

/**
 *  Класс для работы с заказом
 *
 */
class Order {

  /**
   *
   * @var array
   */
  private $order;

  const COOKIE_NAME = "watchdiy2";

  function __construct($presetName = null) {
    $order_id = null;
    if ($_COOKIE[Order::COOKIE_NAME]!=null)
		{
			$cookie = json_decode($_COOKIE[Order::COOKIE_NAME],true);
      $order_id = $cookie['orderId'];
		}
		if ($order_id!=null) {
      $order = Orders::model()->findByPk($order_id);
      if ($order!=null) {
        $this->order = $order;
      }
		}

		if ($this->order==null) {

      $this->newOrder($presetName);
    }
  }

  // public static function clearCookie() {
  //   Yii::app()->request->cookies[Order::COOKIE_NAME] = new CHttpCookie(Order::COOKIE_NAME, null,array('expire'=>time()+60*60*24*30));
  // }

  /**
   * Создаёт новую запись в Orders со статусом "new" и сохраняет её id в куках
   * @return void
   */
  private function newOrder($presetName) {
    $order = new Orders;
    $order->status = "new";
    $order->time_created = new CDbExpression('NOW()');
    $order->time_modified = new CDbExpression('NOW()');
    $order->presetName = $presetName;
    $order->save();
    $this->order = $order;

    $cookie = array("orderId"=>$order->id);

    Yii::app()->request->cookies[Order::COOKIE_NAME] = new CHttpCookie(Order::COOKIE_NAME, json_encode($cookie),array('expire'=>time()+60*60*24*30));
  }

  public function getId() {
    if ($this->order!=null) {
      return $this->order->id;
    }
    return null;
  }

  public function getStatus() {
    if ($this->order!=null) {
      return $this->order->status;
    }
    return null;
  }

  public function getPresetName() {
    if ($this->order!=null) {
      return $this->order->presetName;
    }
    return null;
  }

  /**
   * Возвращает список текущих компонентов часов (их имена)
   * @return array
   */
  public function getWatchComponents() {
    if ($this->order!=null && $this->order->status!="new") {
      $components = array(
        "caseName"=>$this->order->caseName,
        "faceName"=>$this->order->faceName,
        "handsName"=>$this->order->handsName,
        "strapName"=>$this->order->strapName,
        "text"=>json_decode($this->order->text,true),
        "image"=>json_decode($this->order->image,true)
      );
      return $components;
    }
    else {
      return null;
    }
  }

  public function updateWatchComponent($componentName,$componentValue) {
    if ($this->order->status=="new") {
      $this->order->status="edit";
    }
    if ($this->order->status=="checkout") {
      return;
    }

    if ($componentName == "text") {
      $this->order->text = json_encode($componentValue);
    } elseif($componentName == "image") {
      $this->order->image = json_encode($componentValue);
    } else {
      $this->order->$componentName = $componentValue;
    }
    $this->order->update();
  }

  /**
   * Обновить все компоненты разом. (применяется при первоначальном сохранении состояния конструктора часов)
   * @param  array $componentsState состояние компонентов
   * @return void
   */
  public function updateAllWatchComponents($componentsState) {
    $this->discardNewStatus();
    foreach ($componentsState as $componentName=>$componentValue) {
      if ($componentName == "text") {
        $this->order->text = json_encode($componentValue);
      } elseif($componentName == "image") {
        $this->order->image = json_encode($componentValue);
      } else {
        $this->order->$componentName = $componentValue;
      }
    }
    $this->order->update();
  }

  public function setCase($caseName) {
    $this->discardNewStatus();
    $this->order->caseName = $caseName;
    $this->order->update();
  }

  public function setFace($faceName) {
    $this->discardNewStatus();
    $this->order->faceName = $faceName;
    $this->order->update();
  }

  public function setHands($handsName) {
    $this->discardNewStatus();
    $this->order->handsName = $handsName;
    $this->order->update();
  }

  public function setStrap($strapName) {
    $this->discardNewStatus();
    $this->order->strapName = $strapName;
    $this->order->update();
  }

  public function setImage($image) {
    $this->discardNewStatus();
    $this->order->$image = json_encode($image,true);
    $this->order->update();
  }

  public function setText($text) {
    $this->discardNewStatus();
    $this->order->$text = json_encode($text,true);
    $this->order->update();
  }

  private function discardNewStatus() {
    if ($this->order->status=="new")  $this->order->status="edit";
  }

  public function checkout($customerName,$customerPhone,$customerEmail,$customerAddress) {
    $this->order->customerName = $customerName;
    $this->order->customerPhone = $customerPhone;
    $this->order->customerEmail = $customerEmail;
    $this->order->customerAddress = $customerAddress;
    $this->order->status = "checkout";
    $this->order->update();
  }

  public function findByEmail($email) {
    $criteria = new CDbCriteria;
    $criteria->condition = "customerEmail=:email";
    $criteria->params = array(':email'=>$email);
    $criteria->order = "time_created DESC";

    $order = Orders::model()->find($criteria);
    if ($order==null) return false;
    else {
      $this->order = $order;

      $cookie = array("orderId"=>$order->id);
      Yii::app()->request->cookies[Order::COOKIE_NAME] = new CHttpCookie(Order::COOKIE_NAME, json_encode($cookie),array('expire'=>time()+60*60*24*30));
      return true;
    }
  }


}
