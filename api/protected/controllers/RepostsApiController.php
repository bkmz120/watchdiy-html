<?php

class RepostsApiController extends CController {

	public function actionFindVk($id) {
		$repostUrl = RepostsVk::gerRepostUrl($id);
		if ($repostUrl!==false) {
			header('Location: '.$repostUrl);
		}
		else {
			$this->renderPartial('repostSearchResults',array('repostUrl'=>$repostUrl));	
		}		
	}


}