<?php

class WatchConstructorApiController extends CController
{
	public function actionSaveComponentState()
	{
	    $data = json_decode(file_get_contents('php://input'),true);

	    $order = new Order;
	    $response = new AjaxResponse;
	    try {
	      $order->updateWatchComponent($data['componentName'],$data['value']);
	      $response->send();
	    }
	    catch (Exception $e) {
	      $response->setError($e->getMessage());
	      $response->send();
	    }
	}

	public function actionSaveAllComponentsState() {
		$data = json_decode(file_get_contents('php://input'),true);

	    $order = new Order;
	    $response = new AjaxResponse;
	    try {
	      $order->updateAllWatchComponents($data['componentsState']);
	      $response->send();
	    }
	    catch (Exception $e) {
	      $response->setError($e->getMessage());
	      $response->send();
	    }
	}

	public function actionNewWatch() {
		$data = json_decode(file_get_contents('php://input'),true);
		$order = new Order($data['presetName']);
	    $response = new AjaxResponse;
	    $response->setDataItem('orderId',$order->getId());
		$response->send();
	}

	public function actionGetOrder() {
		$order = new Order;
		$components = $order->getWatchComponents();
		$orderStatus = $order->getStatus();
		$presetName = $order->getPresetName();
		$response = new AjaxResponse;
	    if ($components!=null) {
	    	$response->setDataItem('components',json_encode($components));
	    }	    
	    $response->setDataItem('orderStatus',$orderStatus);
	    $response->setDataItem('presetName',$presetName);

		$response->send();
	}

	public function actionFindByEmail() {
		$data = json_decode(file_get_contents('php://input'),true);
		$order = new Order;
		$result = $order->findByEmail($data['email']);

		$response = new AjaxResponse;
		if ($result) {
			$response->setDataItem('orderId',$order->getId());
			$response->setDataItem('presetName',$order->getPresetName());
			$response->setDataItem('components',$order->getWatchComponents());
			$response->setDataItem('orderStatus',$order->getStatus());
		}
		else {
			$response->setError('not found');
		}
		$response->send();
	}

	

}
