<?php

class FormApiController extends CController
{
	//const ADMIN_EMAIL = "order@watchn1.ru";
	const ADMIN_EMAIL = "bkmz120@gmail.com";
	const SERIVCE_EMAIL = "order@watchn1.ru";
	const SMTP_USERNAME = "order@watchn1.ru";
	const SMTP_PASS = "orwat7ru";

	public function actionSendOrderAjax()
	{
		$data = json_decode(file_get_contents('php://input'));
		$data = $data->values;

		$response = new AjaxResponse;

		if (!$this->spamDetect($data)) {
			$response->setError("error");
			$response->send();
			return;
		}

		$order = new Order;
		$order->checkout($data->name,$data->phone,$data->email,$data->address);

		$watchComponents = $order->getWatchComponents();

		$watchImg = str_replace('data:image/png;base64,', '', $data->watchImg);
		$watchImg = str_replace(' ', '+', $watchImg);
		$watchImg= base64_decode($watchImg);

		$ds=DIRECTORY_SEPARATOR;
		$watchImgFileFolder=Yii::app()->basePath.$ds.'..'.$ds.'..'.$ds.'public'.$ds.'userWatchs'.$ds;
		$watchImgFileName = mt_rand().'_'.time().'.png';
		$watchImgFilePath = $watchImgFileFolder.$watchImgFileName;
		file_put_contents($watchImgFilePath,$watchImg);
		$watchImgUrl = "http://watchn1.ru/public/userWatchs/".$watchImgFileName;
		
		if ($data->userImg!=null) {
			$userImgUrl = "http://watchn1.ru/public/userImages/".$data->userImg;
		}				

		try {
			$mailer = new PHPMailer();
			$mailer->CharSet = 'UTF-8';
			$mailer->isSMTP();
			$mailer->SMTPAuth = true;
			$mailer->Host = 'smtp.yandex.ru'; 
			$mailer->Username = self::SMTP_USERNAME;
			$mailer->Password = self::SMTP_PASS;
			$mailer->port = 465;



			/*
			 * Отправка письма админу
			 */
			$htmlMessage = $this->renderPartial('//mailTemplates/successOrderAdminMail',array(
												'orderNum'=>$order->getId(),
												'name'=>$data->name,
												'phone'=>$data->phone,
												'email'=>$data->email,
												'address'=>$data->address,
												'comment'=>$data->comment,
												'caseName'=>$data->case,
												'faceName'=>$data->face,
												'handsName'=>$data->hands,
												'strapName'=>$data->strap,
												'userImgUrl'=>$userImgUrl,
												'userText'=>$data->userText,
												'userTextStyle'=>$data->userTextStyle,
												'watchImgUrl'=>$watchImgUrl,
												'price'=>$data->price,
											),true);

			$mailer->AddAddress(self::ADMIN_EMAIL);
			$mailer->SetFrom(self::SERIVCE_EMAIL);
			$mailer->Subject = 'Новый заказ №'.$order->getId();
			$mailer->Body = $htmlMessage;
			$mailer->IsHTML(true);
			$mailer->AddAttachment($watchImgFilePath,'watch.png');
			if ($data->userImg!=null) {
				$mailer->AddAttachment($userImgPath,'watch-bg.png');
			}		
		
			$res = $mailer->Send();
			if ($res===false) {
				throw new Exception("Send method return false in admin sending");				
			}
			
			/*
			 * Отправка письма покупателю
			 */
			if ($data->email!=null) {
					

				$htmlMessage = $this->renderPartial('//mailTemplates/successOrderUserMail',array(
												'orderNum'=>$order->getId(),
												'caseName'=>$data->case,
												'faceName'=>$data->face,
												'handsName'=>$data->hands,
												'strapName'=>$data->strap,
												'userImgUrl'=>$userImgUrl,
												'userText'=>$data->userText,
												'userTextStyle'=>$data->userTextStyle,
												'watchImgUrl'=>$watchImgUrl,
											),true);				
			
				$mailer->ClearAddresses();
				$mailer->ClearAttachments();
				$mailer->AddAddress($data->email);
				$mailer->Subject = 'Спасибо за заказ №'.$order->getId().' именных часов на сайте watchn1.ru!';

				$mailer->Body = $htmlMessage;
				$mailer->IsHTML(true);
				$mailer->AddAttachment($watchImgFilePath,'watch.png');

				$res = $mailer->Send();	
				if ($res===false) {
					throw new Exception("Send method return false in admin sending");				
				}				
			}
		}
		catch(Exception $e) {
			$response->setError("error while sending email");
		}

		$response->setDataItem('orderId',$order->getId());
		$response->send();
	}



	public function actionSendAskAjax() {
		$data = json_decode(file_get_contents('php://input'));
		$data = $data->values;

		$response = new AjaxResponse;

		if (!$this->spamDetect($data)) {
			$response->setError("error");
			$response->send();
			return;
		}

		$adminText = "";

		$cookie = json_decode($_COOKIE[Order::COOKIE_NAME],true);
		$order_id = $cookie['orderId'];
		if ($order_id!=null) {
			$adminText.= "Заказ №: ".$order_id."\r\n";
			$subject = "Заказ №: ".$order_id." вопрос";
		}
		else {
			$subject = "Новый вопрос";
		}

		$adminText.= "Имя: ".$data->name."\r\n";
		$adminText.= "Email: ".$data->email."\r\n";
		$adminText.= "Текст: ".$data->text."\r\n";



		$response = new AjaxResponse;

		try {
			$mailer = new PHPMailer();
			$mailer->CharSet = 'UTF-8';
			$mailer->isSMTP(); // Set mailer to use SMTP
			$mailer->SMTPAuth = true; // Enable SMTP authentication
			$mailer->Host = 'smtp.yandex.ru'; // Specify main and backup SMTP servers
			$mailer->Username = self::SMTP_USERNAME; // SMTP SMTP_USERNAME
			$mailer->Password = self::SMTP_PASS;
			$mailer->port = 465;
			
			$mailer->AddAddress(self::ADMIN_EMAIL);
			$mailer->SetFrom(self::SERIVCE_EMAIL);
			$mailer->Subject = $subject;
			$mailer->Body = $adminText;			
			$res = $mailer->Send();			
		}

		catch(phpmailerException $e) {
			$response->setError("error while sending email");
		}

		$response->send();
	}


	public function actionSendRepostNotice() {
		$data = json_decode(file_get_contents('php://input'));
		$response = new AjaxResponse;

		if (!$this->spamDetect($data)) {
			$response->setError("error");
			$response->send();
			return;
		}

		$cookie = json_decode($_COOKIE[Order::COOKIE_NAME],true);
		$orderId = $cookie['orderId'];

		$order = Orders::model()->findByPk($orderId);
		if ($order == null) {
			$response->setError('Order not found');
			$response->send();
			return;
		}

		switch ($data->type) {
			case 'vkontakte':
			$repostUrl = "https://vk.com/search?c%5Bq%5D=%23watchn1ru%20%23%D0%BD%D0%BE%D0%BC%D0%B5%D1%80%D0%B7%D0%B0%D0%BA%D0%B0%D0%B7%D0%B0".$orderId."&c%5Bsection%5D=statuses";
			$serviceName = "вконтакте";
			$order->repostVk = 1;
			break;
			case 'facebook':
			$repostUrl = "https://www.facebook.com/search/str/%23watchn1ru+%23%D0%BD%D0%BE%D0%BC%D0%B5%D1%80%D0%B7%D0%B0%D0%BA%D0%B0%D0%B7%D0%B0".$orderId."/keywords_top";
			$serviceName = "facebook";
			$order->repostFb = 1;
			break;	
			case 'twitter':
			$repostUrl = "https://twitter.com/search?q=%23watchn1ru%20%23%D0%BD%D0%BE%D0%BC%D0%B5%D1%80%D0%B7%D0%B0%D0%BA%D0%B0%D0%B7%D0%B0".$orderId."&src=typd";
			$serviceName = "twiiter";
			$order->repostTwitter = 1;
			break;					
		}
		$order->update();

		$text = "Заказ №".$orderId."\n\r";
		$text.= "Проверить репост ".$serviceName." через неколько минут с момента отправки данного сообщения \n\r";
		$text.= $repostUrl." \n\r";
		$text.= "Прежде чем пройти по ссылке, убедитесь что вы совершили вход в ".$serviceName." \n\r";

		

		$mailer = new PHPMailer();
		$mailer->CharSet = 'UTF-8';
		$mailer->isSMTP(); 
		$mailer->SMTPAuth = true; 
		$mailer->Host = 'smtp.yandex.ru';
		$mailer->Username = self::SMTP_USERNAME;
		$mailer->Password = self::SMTP_PASS;
		$mailer->port = 465;
		try {
			$mailer->AddAddress(self::ADMIN_EMAIL);
			$mailer->SetFrom(self::SERIVCE_EMAIL);
			$mailer->Subject = 'Заказ №'.$orderId." репост";
			$mailer->Body = $text;			
			$res = $mailer->Send();			
		}
		catch(phpmailerException $e) {
			$response->setError("error while sending email");
		}

		$response->send();
	}

	//простая проверка
	private function spamDetect($values) {
		if (isset($values->info) && $values->info % 4721 == 0) return true;
		else return false;
	}
}
